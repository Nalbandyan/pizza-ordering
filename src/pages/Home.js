import React, {useState, useEffect} from 'react';
import {Carousel} from 'react-responsive-carousel';
import pizzaData from '../pizzaData.json';
import _ from "lodash"
import Currency from 'react-currency-formatter';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Link} from "react-router-dom";

function Home() {
  let [cartCount, setCount] = useState(0);
  let [currency, setCurrency] = useState("EUR");
  let [orders, setOrders] = useState({});

  useEffect(() => {
    let orders = localStorage.getItem('orders');
    let cartCount = localStorage.getItem('cartCount');
    if (orders) {
      orders = JSON.parse(orders);
      setOrders(orders)
    }
    if (cartCount) {
      cartCount = JSON.parse(cartCount);
      setCount(cartCount)
    }
  },[cartCount]);


  function addToCart(val, action) {
    if (action === 'plus') {
      cartCount = cartCount + 1;
      if (!_.isEmpty(orders) && orders[val.name]) {
        _.set(orders, val.name, orders[val.name] + 1);
      } else {
        _.set(orders, val.name, 1);
      }
    } else {
      if (cartCount !== 0) {
        if (!_.isEmpty(orders) && orders[val.name]) {
          cartCount = cartCount - 1;
          _.set(orders, val.name, orders[val.name] - 1);
        }
      }
    }
    setOrders(orders)
    setCount(cartCount)
    localStorage.setItem('orders', JSON.stringify(orders));
    localStorage.setItem('cartCount', JSON.stringify(cartCount));
  }

  return (
    <>
      <body id="home" data-spy="scroll" data-target=".navbar-collapse">
      <div className="navbar navbar-default navbar-fixed-top" role="navigation">
        <div className="container">
          <div className="navbar-header">
            <button className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span className="icon icon-bar"></span>
              <span className="icon icon-bar"></span>
              <span className="icon icon-bar"></span>
            </button>
            <a href="#home" className="navbar-brand smoothScroll"><strong>PIZZA</strong></a>
          </div>
          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              <li><a href="#home" className="smoothScroll">HOME</a></li>
              <li><a href="#about" className="smoothScroll">ABOUT</a></li>
              <li><a href="#menu" className="smoothScroll">MENU</a></li>
              <li><a href="#contact" className="smoothScroll">CONTACT</a></li>
              <li>
                <Link
                  className="smoothScroll"
                  onClick={() => setCurrency( "USD")}
                >
                  USD
                </Link>
              </li>
              <li>
                <Link
                  className="smoothScroll"
                  onClick={() => setCurrency( "EUR")}
                >
                  EUR
                </Link>
              </li>
              <li><img className="shopping-cart-icon" src="images/shopping-cart.png" alt=""/></li>
              <li><Link className="smoothScroll cart-count">{cartCount}</Link></li>

            </ul>
          </div>
        </div>
      </div>

      <Carousel
        autoPlay
        infiniteLoop
        interval={3000}
        showThumbs={false}
      >
        <li>
          <img src="images/slider-img1.jpg" alt="Pizza Image 1"/>
          <div className="flex-caption">
            <h2 className="slider-title">We make Pizza</h2>
            <h3 className="slider-subtitle">Fresh, clean, and delicious.</h3>
            <p className="slider-description">Praesent tincidunt neque semper elementum gravida. Donec id euismod
              magna. Ut erat ligula, malesuada eu quam a, fringilla auctor augue.</p>
          </div>
        </li>
        <li>
          <img src="images/slider-img2.jpg" alt="Pizza Image 2"/>
          <div className="flex-caption">
            <h2 className="slider-title">Freshly Baked Pizza</h2>
            <h3 className="slider-subtitle">Premium Quality, Finest Ingredients</h3>
            <p className="slider-description">Donec id euismod magna. Ut erat ligula, malesuada eu quam a, fringilla
              auctor augue. Praesent tincidunt neque semper elementum gravida.</p>
          </div>
        </li>
      </Carousel>

      <section id="about" className="templatemo-section templatemo-top-130">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h1 className="text-uppercase">Pizza crust</h1>
            </div>
            <div className="col-md-6 col-sm-6">
              <p>The basic ingredients in pizza crust are yeast, oil, salt, water and flour. Oil helps create a tender
                pizza crust. Olive oil is traditional, but any vegetable oil will work. They all contain healthy fats.
                If you buy ready-made dough, make sure it is trans fat-free.

                Salt creates a higher rising crust, but a pinch is all you need. Most of the sodium in pizza comes
                from the cheese and toppings, not the crust.

                Plain dough is usually made from refined white flour. Whether your order in or make pizza at home, you
                can increase the fibre, vitamins and minerals by choosing whole wheat flour instead.

                Many pizzerias offer whole grain crust. Don’t confuse this with “multigrain.” The word “multi” may
                simply mean “many,” but the dough may contain many refined grains. Ask if it is WHOLE grain. If you
                buy pizza at the grocery store, the first ingredient for the crust should say whole wheat flour – or
                even better, whole grain flour.</p>
            </div>
            <div className="col-md-6 col-sm-6">
              <img src="images/about-img1.jpg" className="img-responsive img-bordered img-about" alt="about img"/>
            </div>
          </div>
        </div>
      </section>

      <section id="menu" className="templatemo-section templatemo-light-gray-bg">
        <div className="container">
          <div className="menu">
            {
              _.map(pizzaData, (levels, key) => {
                return (
                  <div key={key}>
                    <div className="menu-child">
                      {_.map(levels, (val, levelsKey) => {
                        return (
                          <div className="menu-image-container">
                            <img className="pizza-image" src={val.image} alt=""/>
                            <p className="pizza-name">{val.name}</p>
                            <p className="pizza-name">
                              <Currency
                                quantity={val.price[currency]}
                                currency={currency}
                              />
                            </p>
                            <div className="buttons-container">
                              <button
                                onClick={() => addToCart(val, 'minus')}
                                className="action-button shadow animate yellow"
                              >
                                -
                              </button>
                              <input
                                className="order-input"
                                value={!_.isEmpty(orders) ? orders[val.name] : 0}
                                type="text"
                                disabled
                              />
                              <button
                                onClick={() => addToCart(val, 'plus')}
                                className="action-button shadow animate yellow"
                              >
                                +
                              </button>
                            </div>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                )
              })
            }
          </div>
        </div>
      </section>

      <section id="contact" className="templatemo-section">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h2 className="text-uppercase text-center">Contact Us</h2>
              <hr/>
            </div>
            <div className="col-md-2"></div>
            <div className="col-md-8">
              <form action="#" method="post" role="form">
                <div className="col-md-6 col-sm-6">
                  <input name="name" type="text" className="form-control" id="name" maxLength="60"
                         placeholder="Name"/>
                  <input name="email" type="email" className="form-control" id="email" placeholder="Email"/>
                  <input name="message" type="text" className="form-control" id="message" placeholder="Subject"/>
                </div>
                <div className="col-md-6 col-sm-6">
                  <textarea className="form-control" rows="5" placeholder="Message"></textarea>
                </div>
                <div className="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
                  <input name="submit" type="submit" className="form-control" id="submit" value="Send"/>
                </div>
              </form>
            </div>
            <div className="col-md-2"></div>
            <div className="col-md-4 col-sm-4">
              <h3 className="padding-bottom-10 text-uppercase">Visit our shop</h3>
              <p><i className="fa fa-map-marker contact-fa"></i> 63 Another Walking Street, BKK 18080</p>
              <p>
                <i className="fa fa-phone contact-fa"></i>
                <a href="tel:010-020-0340" className="contact-link">010-020-0340</a>,
                <a href="tel:080-090-0660" className="contact-link">080-090-0660</a>
              </p>
              <p><i className="fa fa-envelope-o contact-fa"></i>
                <a href="mailto:hello@company.com" className="contact-link">hello@company.com</a></p>
            </div>
            <div className="col-md-4 col-sm-4">
              <h3 className="text-uppercase">Opening hours</h3>
              <p><i className="fa fa-clock-o contact-fa"></i> 7:00 AM - 11:00 PM</p>
              <p><i className="fa fa-bell-o contact-fa"></i> Monday to Friday and Sunday</p>
              <p><i className="fa fa-download contact-fa"></i>
                <a className="contact-link" rel="nofollow"
                   href="http://fontawesome.io/icons/" target="_blank">Change Icons</a></p>
            </div>
            <div className="col-md-4 col-sm-4">
              <div className="google_map">
                <div id="map-canvas" className="map"></div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <footer>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <p>Copyright &copy; 2084 Company Name</p>
              <hr/>
              <ul className="social-icon">
                <li><a href="#" className="fa fa-facebook"></a></li>
                <li><a href="#" className="fa fa-twitter"></a></li>
                <li><a href="#" className="fa fa-instagram"></a></li>
                <li><a href="#" className="fa fa-pinterest"></a></li>
                <li><a href="#" className="fa fa-google"></a></li>
                <li><a href="#" className="fa fa-github"></a></li>
                <li><a href="#" className="fa fa-apple"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      </body>
    </>
  );
}

export default Home;